#基于Redis实现的分布式锁
1 Redis配置修改
    修改com.vest.distributed.redis.JRedisFactory类下的redisNodes变量
 private static String redisNodes = "192.168.1.51:7001,192.168.1.51:7002,192.168.1.51:7003";
 
2 实例化一个锁
  DistributedLock lock = DistributedLockFactory.builder(lockName);
  lockName为锁的名称，同时对应redis的key，同一个名称代表同一个锁
  
3 单元测试
  DistributedLockTest类
           