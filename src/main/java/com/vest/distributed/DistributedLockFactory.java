package com.vest.distributed;

import com.vest.distributed.impl.RedisDistributedLockImpl;
import com.vest.distributed.redis.JRedisFactory;

/**
 * 创建分布式锁工厂方法
 * @author Liuxianwei
 *
 */
public class DistributedLockFactory {

	/**
	 * 构造一个默认的分布式锁，默认3秒超时
	 * @param lockName
	 * @return
	 */
	public static DistributedLock builder(String lockName){
		RedisDistributedLockImpl lock = new RedisDistributedLockImpl(lockName);
		lock.setRedis(JRedisFactory.builder());
		return lock;
	}
	
	/**
	 * 构造一个分布式锁，以传入的timeout为超时时间
	 * @param lockName
	 * @param timeOut
	 * @return
	 */
	public static DistributedLock builder(String lockName, long timeOut){
		RedisDistributedLockImpl lock = new RedisDistributedLockImpl(lockName, timeOut);
		lock.setRedis(JRedisFactory.builder());
		return lock;
	}
}
