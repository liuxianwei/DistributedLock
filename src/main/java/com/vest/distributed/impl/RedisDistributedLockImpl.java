package com.vest.distributed.impl;

import com.vest.distributed.DistributedLock;

import redis.clients.jedis.JedisCluster;

/**
 * Redis分布式锁实现
 * 设计思路：采用redis set key if not exist功能实现加锁操作，初始化一个锁，
 * 加锁的时候redis会创建一个key=lockName的项目，存入当前的时间戳。当本任务运行完成之后，进行解锁操作
 * 解锁操作是删除本条key=本机记录lockValue的值
 * 如果获得锁的机器意外宕机，那么势必造成死锁情况，这样的会超时机制就发挥应有的作用，当出现超时的情况就认为是有死锁产生
 * 那么这个记录将会被删除，其余线程进行锁的抢占。
 * 程序是用时间戳进行加锁操作，所以要求所有的机器时间必须同步
 * @author Liuxianwei
 *
 */
public class RedisDistributedLockImpl implements DistributedLock {

	private JedisCluster redis;
	
	private String lockName;
	private String lockValue;
	private long timeOut;
	
	public static final long DEFAULT_TIMEOUT = 3000L; //默认超时时间，毫秒为单位
	
	public RedisDistributedLockImpl(String lockName){
		this(lockName, DEFAULT_TIMEOUT);
	}
	
	public RedisDistributedLockImpl(String lockName, long timeOut){
		this.lockName = lockName;
		this.timeOut = timeOut;
	}
	
	public void lock() {
		int count = 0;
		for(;;){
			lockValue = getTime() + "";
			Long result = redis.setnx(lockName, lockValue);
			if(result == 1){
				break;
			}
			if(isTimeOut() && count++ % 20 == 0){//没20次检测一次
				redis.del(lockName);
			}
			try {
				Thread.sleep(3L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean tryLock() {
		lockValue = getTime() + "";
		Long result = redis.setnx(lockName, lockValue);
		if(result == 1){
			return true;
		}
		if(isTimeOut()){
			redis.del(lockName);
		}
		return false;
	}

	public boolean tryLock(int timeOut) {
		boolean flag = false;
		Long time = System.currentTimeMillis();
		int count = 0;
		for(;;){
			lockValue = getTime() + "";
			Long result = redis.setnx(lockName, lockValue);
			if(result == 1){
				flag = true;
				break;
			}
			if(isTimeOut() && count++ % 20 == 0){//没20次检测一次
				redis.del(lockName);
			}
			try {
				Thread.sleep(3L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			Long now = System.currentTimeMillis();
			if((now - time) >= timeOut){
				break;
			}
		}
		return flag;
	}

	public void unLock() {
		String oldValue = redis.get(lockName);
		if(lockValue.equals(oldValue)){
			redis.del(lockName);
		}
	}
	
	private boolean isTimeOut(){
		String oldValue = redis.get(lockName);
		if(oldValue == null){
			return false;
		}
		long now = System.currentTimeMillis();
		long useTime = now - Long.parseLong(oldValue);
		if(useTime >= timeOut){
			return true;
		}
		return false;
	}
	
	/**
	 * 获取当前时间戳，如果无法做到所有的机器时间同步，
	 * 可以采用一个机器提供时间服务，这里调用一个时间服务保证所有机器时钟同步
	 * @return 时间戳
	 */
	private long getTime(){
		return System.currentTimeMillis();
	}

	public void setRedis(JedisCluster redis) {
		this.redis = redis;
	}
}
