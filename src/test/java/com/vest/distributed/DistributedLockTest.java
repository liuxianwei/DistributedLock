package com.vest.distributed;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;

public class DistributedLockTest {

	private static final String lockName = "epay:lockTest";
	
	private int count = 0;
	
	private long start = 0;
	private long end = 0;
	@Test
	public void testLock() throws IOException{
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(5);  
		for(int i = 0; i < 10000; i++){
			fixedThreadPool.execute(new Task("woker_" + i));
		}
		System.in.read();
		System.out.println("耗时：" + (end-start)/1000);
		System.out.println("count:" + count);
	}
	
	class Task implements Runnable{

		private String name;
		private DistributedLock lock;
		
		public Task(String name){
			this.name = name;
			lock = DistributedLockFactory.builder(lockName, 10000);
		}
		
		public void run() {
			try{
				if(start == 0){
					start = System.currentTimeMillis();
				}
				lock.lock();
				System.out.println(this.name + "开始工作了");
				//int time = (int) (Math.random() * 5);
				int time = 5000;
				count++;
				if(time > 0){
					Thread.sleep(time);
				}
				long now = System.currentTimeMillis();
				if(end < now){
					end = now;
				}
				System.out.println(this.name + "结束工作了;" + System.currentTimeMillis());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			finally{
				lock.unLock();
			}
		}
		
	}
}
